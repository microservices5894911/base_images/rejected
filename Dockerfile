FROM mvijayaragavan/python3:3.9.16-0

LABEL url="https://gitlab.com/microservices5894911/base_images/rejected.git"

ARG REJECTED_VERSION=3.22.0

COPY . /

COPY /etc/rejected_template.yaml /usr/local/bin/rejected_template.yaml

RUN apk --update add c-ares curl \
 && apk --virtual devdeps add curl curl-dev gcc musl-dev linux-headers \
 && pip3 install -U pip setuptools wheel \
 && pip3 install json-scribe PyYAML yarl pycurl rejected[sentry]==${REJECTED_VERSION} \
 && pip3 freeze > /tmp/installed-packages.txt \
 && apk del --purge devdeps \
 && rm -rf /var/cache/apk/*

CMD ["/entry_point.sh"]
