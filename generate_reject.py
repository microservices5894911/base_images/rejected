import copy
import json
import os

import yaml
from yarl import URL


def generate_rabbitmq_config():
    ampq_url = os.environ.get('AMQP_URL')
    assert ampq_url, 'Set AMPQ url at env "AMQP_URL"'

    rejected_consumers = os.environ.get('REJECTED_CONSUMERS')
    assert rejected_consumers, 'Set consumers at env "REJECTED_CONSUMERS"'


    rejected_consumers = json.loads(rejected_consumers)
    ssl_enabled = os.environ.get('is_ssl_enabled', False)

    url = URL(ampq_url)

    with open('/usr/local/bin/rejected_template.yaml') as rejected:
        rejected_template = copy.copy(yaml.safe_load(rejected))
        application = rejected_template['Application']
        rabbit_connection = application['Connections']['rabbitmq']
        rabbit_connection['host'] = url.host
        rabbit_connection['port'] = url.port
        rabbit_connection['user'] = url.user
        rabbit_connection['pass'] = url.password
        rabbit_connection['ssl'] = ssl_enabled
        application['Consumers'] = rejected_consumers

    with open(r'/usr/local/bin/rejected.yaml', 'w') as file:
        yaml.dump(rejected_template, file)


if __name__ == "__main__":
    generate_rabbitmq_config()
